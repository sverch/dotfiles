# Dotfiles

Run `install.sh` to install.  This will back up your old dotfiles if they
already exist.

## Firefox Setup

Just to write this down somewhere...  I followed
https://support.mozilla.org/en-US/questions/1185935 to hide my tab bar because
I'm using tree style tab.  It involves creating a `userChrome.css` file in my
profile that has the following in it:

```
#TabsToolbar {
visibility: collapse;
}

#titlebar {
display: none;
}
```

This is at `~/.mozilla/<profile>/chrome/userChrome.css`.

Also, I need
[this](https://www.reddit.com/r/archlinux/comments/8po7wd/finally_fixed_firefoxs_slow_scrolling_and_it_is/)
to fix the firefox slow scroll issue.

## Backlight Control Setup

This is something I should add at some point.  But following the steps here
fixes it:
https://askubuntu.com/questions/715306/xbacklight-no-outputs-have-backlight-property-no-sys-class-backlight-folder

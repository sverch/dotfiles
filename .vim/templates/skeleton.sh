#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

NUM_ARGS_REQUIRED=1
if [ $# -ne "${NUM_ARGS_REQUIRED}" ]; then
    cat <<EOF
Usage: $0 <argument_one>

    Usage documentation goes here

EOF
    exit 1
fi

run () {
    echo "+" "$@" 1>&2
    "$@"
}

color () {
    COLOR=$1
    MESSAGE=$2
    case "${COLOR}" in
    red)
        echo -e "\e[31m${MESSAGE}\e[39m"
        ;;
    blue)
        echo -e "\e[94m${MESSAGE}\e[39m"
        ;;
    green)
        echo -e "\e[32m${MESSAGE}\e[39m"
        ;;
    *)
        echo "Unrecognized color: ${COLOR}" 1>&2
        echo -e "${MESSAGE}"
        ;;
    esac
}

ARGUMENT_ONE=$1

color red "${ARGUMENT_ONE}"
color green "${ARGUMENT_ONE}"
color blue "${ARGUMENT_ONE}"
color unknown "${ARGUMENT_ONE}"

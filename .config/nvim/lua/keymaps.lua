local function keymap(mode, lhs, rhs, opts)
    local options = { noremap = true, silent = true }
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end
    vim.keymap.set(mode, lhs, rhs, options)
end

-- Quick exit without saving
keymap('n', 'Q', ':q<CR>', { desc = "Quick exit without saving" })

-- Faster vertical scrolling
keymap({ 'n', 'v' }, '<C-k>', '10k', { desc = "Scroll up ten lines" })
keymap({ 'n', 'v' }, '<C-j>', '10j', { desc = "Scroll down ten lines" })

-- Switching tabs
keymap('n', '<C-l>', 'gt', { desc = "Go to next tab" })
keymap('n', '<C-h>', 'gT', { desc = "Go to previous tab" })

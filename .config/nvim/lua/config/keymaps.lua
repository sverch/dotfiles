-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- Fast Scrolling
vim.keymap.set({ "n", "v" }, "<C-j>", "10j")
vim.keymap.set({ "n", "v" }, "<C-k>", "10k")

-- Switching Tabs
vim.keymap.set({ "n", "v" }, "<C-l>", "<cmd>tabnext<cr>")
vim.keymap.set({ "n", "v" }, "<C-h>", "<cmd>tabprevious<cr>")

-- Quick Exit
vim.keymap.set("n", "Q", ":q<CR>")

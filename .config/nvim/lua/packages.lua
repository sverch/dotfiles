-- NOTE: This needs to be first, see:
-- https://github.com/ms-jpq/coq_nvim/issues/403#issuecomment-1584999754
vim.g.coq_settings = {
  keymap = {
    jump_to_mark = '<c-n>',
  },
  auto_start = 'shut-up',
}

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
  {
    'mrcjkb/rustaceanvim',
    version = '^4',
    ft = { 'rust' },
  },
  {
    'neovim/nvim-lspconfig',
  },
  {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
  },
  {
    'ms-jpq/coq_nvim',
    branch = 'coq',
    config = function()
      -- vim.g.coq_settings = { auto_start = 'shut-up' }
      require("coq")
    end,
  },
  {
    'tpope/vim-sleuth'
  },
  -- 9000+ Snippets
  {
    'ms-jpq/coq.artifacts',
    branch = 'artifacts',
  },
  -- Third party sources -- See https://github.com/ms-jpq/coq.thirdparty
  {
    'ms-jpq/coq.thirdparty',
    branch = '3p',
  },
  {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = true,
  },
  {
    "NeogitOrg/neogit",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "nvim-telescope/telescope.nvim",
    },
    config = true
  },
  {
    "airblade/vim-gitgutter",
  },
  {
    "pmizio/typescript-tools.nvim",
  }
}

-- TODO: Figure out how to configure this
local configs = require("lspconfig.configs")
configs['rustaceanvim'] = {
  default_config = {}
}

require("lazy").setup(plugins)

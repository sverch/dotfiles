return {
  "conform.nvim",
  opts = {
    formatters = {
      shfmt = {
        prepend_args = function(_, _)
          return { "-i", "4" }
        end,
      },
    },
  },
}

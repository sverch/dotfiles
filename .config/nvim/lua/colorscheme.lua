vim.o.termguicolors = false
require('tokyonight').setup({
  style = "night",
  on_colors = function(colors)
    colors.bg = "#000000"
    colors.fg = colors.orange
    colors.comment = colors.blue
    colors.gitSigns.add = colors.green1
    colors.gitSigns.delete = colors.red
    colors.gitSigns.change = colors.cyan
    colors.fg_gutter = colors.yellow
  end,
})
require('tokyonight').colorscheme()

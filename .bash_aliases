#!/bin/bash

alias vim='nvim'

# https://unix.stackexchange.com/questions/25327/watch-command-alias-expansion
alias watch='watch '
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Get platform information, since how we alias ls to do color output is platform dependent
platform='unknown'
unamestr=$(uname)
if [[ "$unamestr" == 'Linux' ]]; then
   platform='linux'
elif [[ "$unamestr" == 'FreeBSD' ]]; then
   platform='freebsd'
fi

# ls color aliases
if [[ $platform == 'linux' ]]; then
   alias ls='ls --color=auto'
elif [[ $platform == 'freebsd' ]]; then
   alias ls='ls -G'
fi

# Alias vim to vimx if it exists.  It's essentially vim with xterm clipboard support.
if hash vimx 2>/dev/null; then
    alias vim='vimx'
fi

# Local aliases that aren't in version control 
if [ -f ~/.bash_site_aliases ]; then
    # shellcheck source=/dev/null
    . ~/.bash_site_aliases
fi

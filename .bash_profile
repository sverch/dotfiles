# shellcheck shell=bash

# Get the aliases and functions
if [ -f "${HOME}/.bashrc" ]; then
    # shellcheck source=/dev/null
	. "${HOME}/.bashrc"
fi
export PATH

. "$HOME/.cargo/env"

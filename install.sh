#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

# https://stackoverflow.com/a/246128
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

run () {
    echo "+" "$@" 1>&2
    "$@"
}

color () {
    COLOR=$1
    MESSAGE=$2
    case "${COLOR}" in
    red)
        echo -e "\e[31m${MESSAGE}\e[39m"
        ;;
    blue)
        echo -e "\e[94m${MESSAGE}\e[39m"
        ;;
    green)
        echo -e "\e[32m${MESSAGE}\e[39m"
        ;;
    *)
        echo "Unrecognized color: ${COLOR}" 1>&2
        echo -e "${MESSAGE}"
        ;;
    esac
}

ensure_symlink() {
    local src="$1"
    local dest="$2"
    color green "Checking if \"${dest}\" is installed."
    if [[ -e "${dest}" ]]; then
        if [[ -L "${dest}" ]]; then
            if [[ "$(readlink -f "${dest}")" != "${src}" ]]; then
                color green "\"${dest}\" is a symlink to \"$(readlink -f "${dest}")\"."
                color green "Deleting \"${dest}\""
                run rm -f "${dest}"
                color green "Symlinking \"${dest}\" to \"${src}\""
                run ln -s "${src}" "${dest}"
            fi
        else
            color green "\"${dest}\" already exists and is not a symlink!"
            color green "Backing up to \"${dest}.$(date +%s%N)\"..."
            run mv "${dest}" "${dest}.$(date +%s%N)"
            color green "Symlinking \"${dest}\" to \"${src}\""
            run ln -s "${src}" "${dest}"
        fi
    else
        if [[ -L "${dest}" ]]; then
	    # Broken Symlink
            if [[ "$(readlink -f "${dest}")" != "${src}" ]]; then
                color green "\"${dest}\" is a symlink to \"$(readlink -f "${dest}")\"."
                color green "Deleting \"${dest}\""
                run rm -f "${dest}"
                color green "Symlinking \"${dest}\" to \"${src}\""
                run ln -s "${src}" "${dest}"
            fi
        else
            color green "Symlinking \"${dest}\" to \"${src}\""
            run ln -s "${src}" "${dest}"
        fi
    fi
}

for dotfile_src in "${SCRIPT_DIR}"/.*; do
    dotfile=$(basename "${dotfile_src}")
    dotfile_dest="${HOME}/${dotfile}"
    if [[ "${dotfile}" == "." ]] ||
        [[ "${dotfile}" == ".." ]] ||
        [[ "${dotfile}" == ".git" ]] ||
        [[ "${dotfile}" == ".gitignore" ]] ||
        [[ "${dotfile}" == ".config" ]]; then
        continue
    fi
    ensure_symlink "${dotfile_src}" "${dotfile_dest}"
done

for config_dir_src in "${SCRIPT_DIR}"/.config/*; do
    config_dir=$(basename "${config_dir_src}")
    config_dir_dest="${HOME}/.config/${config_dir}"
    ensure_symlink "${config_dir_src}" "${config_dir_dest}"
done

color green ""
color green "Home dir bin scripts installed!"
color green ""

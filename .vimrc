" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

call plug#begin('~/.vim/plugged')
Plug 'airblade/vim-gitgutter', { 'branch': 'main' }
Plug 'cespare/vim-toml', { 'branch': 'main' }
Plug 'chazy/cscope_maps'
Plug 'derekwyatt/vim-scala'
Plug 'dhruvasagar/vim-table-mode'
Plug 'editorconfig/editorconfig-vim'
Plug 'hashivim/vim-vagrant'
Plug 'othree/html5.vim'
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'
Plug 'terryma/vim-multiple-cursors'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-ruby/vim-ruby'
Plug 'vim-scripts/DrawIt'
Plug 'vim-scripts/cscope.vim'
Plug 'dhruvasagar/vim-marp'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'stephpy/vim-yaml'
Plug 'jez/vim-superman'
Plug 'scrooloose/nerdtree'
Plug 'lervag/vimtex'
Plug 'fatih/vim-go'
Plug 'hashivim/vim-terraform'
Plug 'vim-syntastic/syntastic'
Plug 'git://git.wincent.com/command-t.git'
Plug 'z0mbix/vim-shfmt', { 'for': 'sh' }
Plug 'junegunn/vim-easy-align'
call plug#end()

"""
""" 'airblade/vim-gitutter'
"""
" Delay before the vim-gitgutter updates.  See:
" https://github.com/airblade/vim-gitgutter#getting-started
set updatetime=100

"""
""" 'scrooloose/nerdtree'
"""
nmap <silent> <C-b> :NERDTree<CR><C-w><C-w>:q<CR>:colorscheme elflord<CR>

"""
""" 'git://git.wincent.com/command-t.git'
"""
" Command-T leader mapping
let mapleader = ',' "use , instead of \ as the 'leader' key (used in some plugins)
" Keybinds for scrolling in Command-T plugin
let g:CommandTSelectNextMap=['<C-n>', '<C-j>', '<Down>', '<Tab>']
let g:CommandTSelectPrevMap=['<C-p>', '<C-k>', '<Up>', '<S-Tab>']
" Ignore directories for Command-T
set wildignore=docs/*,build/*,debian/*

"""
""" Language Settings
"""

" Latex!
let g:vimtex_compiler_enabled=0

" Golang!
let g:go_auto_type_info = 1
" Go to definition in a new tab!
autocmd FileType go nmap <silent> <Leader>d <Plug>(go-def-tab)

" Rust ctags
autocmd BufRead *.rs :setlocal tags=./rusty-tags.vi;/

" Terraform!
let g:terraform_fmt_on_save=1

" Shell Scripts
let g:shfmt_fmt_on_save = 1

" Syntastic!
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_sass_checkers=["sass_lint"]
let g:syntastic_scss_checkers=["sass_lint"]
let g:syntastic_go_checkers = ['go', 'golint', 'govet', 'gopls', 'errcheck', 'godoc']
let g:syntastic_python_checkers = ['python', 'pylint']
let g:syntastic_yaml_checkers = ['yamllint']
let g:syntastic_sh_shellcheck_args="-x"

noremap <C-F> :Files<CR>

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" General "
"""""""""""

" Underline current line with '=' (Useful for markdown)
command! Underline t.|s/./=/g

" Remember last 1024 commands
set history=1024

" Always report number of lines altered
set report=0

" Lots of undo
set undolevels=4096

" Mode lines are scary (arbitrary command execution!)
set nomodeline

" What to show when I hit :set list
set listchars=tab:\|\ ,trail:.,extends:>,precedes:<,eol:$

" Automatically reread files that have been updated
set autoread

" Show line numbers
set number

" When clearing a word, put a dollar sign at the end of cleared
" text instead of clearing immediately
set cpoptions+=$

" Allow backspacing over everything in insert mode
set backspace=indent,eol,start

" Save with ctrl-s
nnoremap <C-s> :wa<CR>

" Always show status line
set laststatus=2

" Color Highlighting "
""""""""""""""""""""""
" Enable syntax highlighting
syntax on

" Sets all colors for various elements (comments, code, keywords, etc.)
colorscheme elflord

" Turn off highlighting for Sign Column so vim-gitgutter doesn't look weird
highlight clear SignColumn

" Turn on highlighting of extra whitespace at the end of line
autocmd InsertEnter * syn clear EOLWS | syn match EOLWS excludenl /\s\+\%#\@!$/
autocmd InsertLeave * syn clear EOLWS | syn match EOLWS excludenl /\s\+$/
highlight EOLWS ctermbg=red guibg=red

" Enable syntax highlighting in redo's *.do files
au BufNewFile,BufRead *.do set filetype=sh

" Json syntax highlighting and indentation
au! BufRead,BufNewFile *.json set filetype=json

" Yaml indentation
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" Less syntax highlighting
au BufNewFile,BufRead *.less set filetype=less

" Wrap git commit body at 72 characters
au FileType gitcommit setlocal tw=72

" Make visual selections work by inverting fg and bg
" See
" https://github.com/tarruda/vim-files/blob/master/user/colors/transparent.vim
" for examples of how to control colors here.
hi Visual cterm=inverse

" Code "
""""""""
" Show matching brackets
set showmatch
"auto close {
inoremap {<Enter> {<Enter>}<Esc>O
" fold code between markers
set foldenable
set foldmarker={{{,}}}
set foldmethod=marker
set foldopen=block,hor,mark,percent,quickfix,search,tag,undo
map <Space> za
function! FoldText()
    let lines = 1 + v:foldend - v:foldstart
    let ind = indent(v:foldstart)
    let spaces = ''
    let i = 0
    while i < ind
        let i = i+1
        let spaces = spaces.' '
    endwhile
    let linestxt = 'lines'
    if lines == 1
        linestxt = 'line'
    endif

    return spaces.'+'.v:folddashes.' '.lines.' '.linestxt.':'
endfunction
set foldtext=FoldText()
"a cool debugging line (hit _if in 'normal' (not insert) mode to try it)
autocmd! FileType cpp
    \ nnoremap _if ostd::cout << __FILE__ << " " << __LINE__  << " " << __FUNCTION__ << " - " << std::endl;<Esc>F"i
nnoremap _if ofprintf(0<C-d>stderr, "{%s}:{%d} - \n", __FILE__, __LINE__);<Esc>F\i
nnoremap _st oif (!ret.isOK()) {<CR>return ret;<CR>}<Esc>F"i

" Templates "
"""""""""""""

" Auto fill new .sh files with a template
if has("autocmd")
  augroup templates
    autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh
  augroup END
endif

" Searching "
"""""""""""""
set ignorecase         " Do case insensitive matching
set smartcase          " Case insensitive unless capital letters are entered
set incsearch          " Incremental search
set hlsearch           " Highlight search matches
" When searching, bring result to middle of screen
map N Nzz
map n nzz
" Cscope/Ctags
map <C-]> <C-]>zz
map <C-t> <C-t>zz

" TODO: set up easytags on vim
" TODO: also see tagselect.vim
"make <C-]> use :tagjump
nnoremap <C-]> g<C-]>

" Indentation "
"""""""""""""""

set autoindent          "continue current indent on the following line
set smartindent         "do the Right Thing
set nocindent           "use indent scripts
set expandtab           "tab key -> spaces
set shiftwidth=4        "indent by 4 spaces
set shiftround          "round indent to multiples of shiftwidth
set tabstop=4           "tab characters are drawn as 4 spaces
set softtabstop=4       "treat 4 spaces like a tab
set smarttab            "backspace deletes a tab's worth of spaces

" Better shift indentation (keeps selection)
vnoremap > >gv
vnoremap < <gv

" Load filetype specifc plugins
filetype plugin on

" Have Vim load indentation rules according to the detected filetype. Per
" default Debian Vim only load filetype specific plugins.
if has("autocmd")
  filetype indent on
endif

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
"filetype plugin indent on


" Cursor "
""""""""""
set scrolloff=5 "try to keep at least 5 lines above and bellow the cursor
set cursorline "underline line the cursor is on

" Faster vertical scrolling
map <C-k> 10k
map <C-j> 10j

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
autocmd! BufReadPost *
  \ if line("'\"") > 0 && line("'\"") <= line("$") |
  \   exe "normal g`\"" |
  \ endif

" Cut/Copy/Paste "
""""""""""""""""""
" Make shift Y behave like shift-[cd] (copy to end of line)
nnoremap Y y$

" Make paste replace selected text in visual mode
vnoremap p "_dp

" By default copy/paste with the X11 clipboard ("* register)
set clipboard+=unnamed

" Change inside of a (single-line) string, see :help objects
nnoremap c' ci'
nnoremap c" ci"
nnoremap d' da'
nnoremap d" da"

" Make pasting work with CTRL-V for convenience
inoremap <C-v> <ESC>"+pi

" Command Mode "
""""""""""""""""""""""
" Dont require a shift to enter command mode
nnoremap ; :
" Enable menu for tab completion in cmd mode
set wildmenu
set wildmode=list,longest
set wildmenu " Show possible completions in command (:) mode (try hitting tab twice)
set wildmode=list:longest,full " Make the wildmenu behave more like bash
set wildignore+=*.o,*.git,*.svn " Ignore these files
set wildignore+=*/build/*,*/third_party/* " Ignore these dirs
set nostartofline " Don't go to the start of line after certain commands
set autowrite          " Automatically save before commands like :next and :make

"use readline maps in command mode
cnoremap <C-a> <HOME>
cnoremap <C-e> <END>

" Text Alignment "
""""""""""""""""""
set textwidth=80       " Used to line break comments (and text if :set fo+=t)
set formatoptions=rocq " Line break oomments and understand how to 'gqap' comments
set linebreak          " Only break lines at word boundaries

" Highlight the 101 char column in cpp files
autocmd FileType cpp set colorcolumn=101

" Set noexpandtab in haskell files because the interpreter doesn't like spaces
autocmd FileType haskell set noexpandtab

" Set line length to 100 for python
autocmd FileType python set textwidth=100

" Align inline comments
noremap + :Tabularize /\/\/<CR>

" Don't use Ex mode, use Q to exit
map Q :q<CR>

" Tabs "
""""""""
" Switching tabs
map <C-h> gT
map <C-l> gt

" Shifting tabs left and right
nmap <C-m> :call MoveTabRight()<CR>
nmap <C-n> :call MoveTabLeft()<CR>

function! MoveTabLeft()
let n = tabpagenr()
execute 'tabmove'(n==1?tabpagenr('$'):n-2)
let &showtabline=&showtabline
endfunction

function! MoveTabRight()
let n = tabpagenr()
execute 'tabmove'(n==tabpagenr('$')?0:n)
let &showtabline=&showtabline
endfunction

" Windows "
"""""""""""

" Buffers "
"""""""""""

" Hide buffers when they are abandoned
set hidden

" Mouse "
"""""""""
set mouse=a     " Enable mouse usage (all modes) in terminals
set mousehide   " Hide the mouse when typing text

" Plugins "
"""""""""""

" https://gist.github.com/csswizardry/9a33342dace4786a9fee35c73fa5deeb
noremap f :tabnew **/*

" Don't create netrwhist, I already have .vim in version control
" https://stackoverflow.com/questions/9850360/what-is-netrwhist
let g:netrw_dirhistmax = 0

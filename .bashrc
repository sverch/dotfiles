# shellcheck shell=bash

# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

#-------------------------------------------------------------
# Alias settings
#-------------------------------------------------------------

if [ -f ~/.bash_aliases ]; then
    # shellcheck source=/dev/null
    . ~/.bash_aliases
fi

#-------------------------------------------------------------
# Readline (line editing) settings
#-------------------------------------------------------------

# Set Readline into vi mode
set -o vi

#-------------------------------------------------------------
# Autocomplete settings
#-------------------------------------------------------------

# Enable programmable completion features (you don't need to enable this if it's
# already enabled in /etc/bash.bashrc and /etc/profile sources
# /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    # shellcheck source=/dev/null
    . /etc/bash_completion
fi

#-------------------------------------------------------------
# History settings
#-------------------------------------------------------------

# Eternal bash history.
# https://stackoverflow.com/questions/9457233/unlimited-bash-history#19533853
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

# append to the history file, don't overwrite it
shopt -s histappend

# Save multiline commands in single history entry
shopt -s cmdhist

# Options that work with history substitution:
# https://www.gnu.org/software/bash/manual/bash.html#History-Interaction
# histverify will let you edit the command that results from doing a
# substitution, and histreedit will let you edit the substitution if it fails
# (like referencing a line in your history that doesn't exist).
shopt -s histreedit histverify

#-------------------------------------------------------------
# General usability settings
#-------------------------------------------------------------

# Prevent overwriting of files by redirection (> will error if the file exists,
# and >| can be used to explicitly signal that you want to clobber the file).
set -o noclobber

# Notify when jobs running in the background terminate
set -o notify

# Do not immediately exit on EOF (CTRL-D) in an empty prompt
set -o ignoreeof

# Do not tab complete on empty line
shopt -s no_empty_cmd_completion

# Update rows and columns on window resize
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Color stderr output red
color() (
    set -o pipefail
    "$@" 2>&1 >&3 | sed $'s,.*,\e[31m&\e[m,' >&2
) 3>&1

# Allow the user to set the window title
function set-title {
    PROMPT_COMMAND="echo -ne \"\\033]0;$1 (on $HOSTNAME)\\007\""
}

export EDITOR="vim"
export PAGER="less"

#-------------------------------------------------------------
# Color settings
#-------------------------------------------------------------

# Set terminal to support 256 colors
if [ -e /usr/share/terminfo/x/xterm-256color ]; then
    export TERM='xterm-256color'
else
    export TERM='xterm-color'
fi

#-------------------------------------------------------------
# Prompt settings
#-------------------------------------------------------------

# https://stackoverflow.com/questions/22322879/how-to-print-current-bash-prompt
# https://stackoverflow.com/questions/17368067/length-of-string-in-bash
# https://superuser.com/questions/380772/removing-ansi-color-codes-from-text-stream
function __wrap_prompt_if_too_long {
    # shellcheck disable=SC2016
    PROMPT='\[\033[0;32m\][\[\033[0m\]\u@\h \w$(__git_ps1 " (%s)")\[\033[0;32m\]]\[\033[0m\]\$ '
    EXPANDED_PROMPT="${PROMPT@P}"
    # shellcheck disable=SC2001
    EXPANDED_PROMPT_NOCOLOR="$(echo "${EXPANDED_PROMPT}" | sed 's/\x1b\[[0-9;]*m//g')"
    if [ ${#EXPANDED_PROMPT_NOCOLOR} -gt "$(("$(tput cols)" - 30))" ]; then
        # shellcheck disable=SC2016
        PROMPT='\[\033[0;32m\][\[\033[0m\]\u@\h \w$(__git_ps1 " (%s)")\[\033[0;32m\]]\[\033[0m\]\n \$ '
        EXPANDED_PROMPT="${PROMPT@P}"
        echo "$EXPANDED_PROMPT"
    else
        echo "$EXPANDED_PROMPT"
    fi
}

export PS1='$(__wrap_prompt_if_too_long)'

#-------------------------------------------------------------
# BSD Settings
#-------------------------------------------------------------

# apparently BSD doesn't set up ls colors the same way.  Not surprising.
# Mac OSX uses this method as well.
export CLICOLOR=YES
export LSCOLORS=ExGxFxdxCxDxDxhbadExEx

#Tips
#fortune freebsd-tips

#-------------------------------------------------------------
# Man Settings
#-------------------------------------------------------------
# View man pages in vim!
#
# I believe had originally had this as an alias and wrapper script because the "PAGER" or "MANPAGER"
# environment variables do not accept pipes.  This is solved by wrapping everything in a call to the
# "/bin/sh" shell.  Source: http://zameermanji.com/blog/2012/12/30/using-vim-as-manpager/
#
# Okay, apparently "MANPAGER" has some problems.  On some platforms this works fine, but on other
# platforms it fails because the output that "man" passes to the "MANPAGER" command is different
# from what it passes to a pipe.  Using the alias and wrapper script works for now.
#export MANPAGER="/bin/sh -c \"col -b | vim -c 'set ft=man nomod nolist' -c 'map q :q<CR>' -c 'set nonumber' -\""
export MANPAGER="cat"

#-------------------------------------------------------------
# Plugins (home brewed plugin structure so I can put in git)
#-------------------------------------------------------------

# Load bash plugins
if [ -d ~/.bash_plugins ]; then
    for plugin in ~/.bash_plugins/*/*; do
        # shellcheck source=/dev/null
        . "$plugin"
    done
fi

if [ -n "$BASH_VERSION" ] || [ -n "$ZSH_VERSION" ]; then
    CHRUBY_HOME="/usr/local/share/chruby"
    if [ -f "$CHRUBY_HOME/chruby.sh" ]; then
        # shellcheck source=/dev/null
        source "$CHRUBY_HOME/chruby.sh"
    fi
    if [ -f "$CHRUBY_HOME/auto.sh" ]; then
        # shellcheck source=/dev/null
        source "$CHRUBY_HOME/auto.sh"
    fi
fi

#-------------------------------------------------------------
# Local Path Setup
#-------------------------------------------------------------
# My local user scripts
PATH=$PATH:$HOME/bin
export GOPATH=$HOME/go/
export PATH=$PATH:${GOPATH//://bin:}/bin
export PATH=$PATH:/usr/local/go/bin
export PATH="$HOME/.tfenv/bin:$PATH"
export PATH="$HOME/.poetry/bin:$PATH"
export PATH="$HOME/.pyenv/bin:$PATH"
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
export PATH="$HOME/.cargo/bin:$PATH"

# Needed for things like pip install --user
# NOTE: Install this before the other things in $PATH so we can do overrides
PATH=$HOME/.local/bin:$PATH

### GCloud Configuration ###

# The next line updates PATH for the Google Cloud SDK.
# shellcheck source=/dev/null
if [ -f '/opt/google-cloud-sdk/completion.bash.inc' ]; then . '/opt/google-cloud-sdk/completion.bash.inc'; fi
# shellcheck source=/dev/null
if [ -f '/opt/google-cloud-sdk/path.bash.inc' ]; then . '/opt/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
# shellcheck source=/dev/null
if [ -f '/opt/google-cloud-sdk/completion.bash.inc' ]; then . '/opt/google-cloud-sdk/completion.bash.inc'; fi

### FZF Configuration ###

# Options to fzf command
export FZF_COMPLETION_OPTS='+c -x'

# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# First, make sure we normalize the command if we installed via the ubuntu
# package manager.
FDFIND="fd"
if ! command -v fd &>/dev/null; then
    if command -v fdfind &>/dev/null; then
        FDFIND="fdfind"
    fi
fi
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
    "$FDFIND" --hidden --follow --exclude ".git" --exclude ".snapshots" --exclude ".backups" . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
    "$FDFIND" --type d --hidden --follow --exclude ".git" --exclude ".snapshots" --exclude "backups" . "$1"
}

export FZF_DEFAULT_COMMAND="$FDFIND --exclude .snapshots --exclude .backups"

# To apply the command to CTRL-T as well
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# shellcheck source=/dev/null
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Include vim man pages, see: https://github.com/jez/vim-superman
# You must have already run Vim Plug installation on your vim that has this
# plugin in its vimrc.  See: https://github.com/junegunn/vim-plug
export PATH="$PATH:$HOME/.vim/plugged/vim-superman/bin"
alias man='vman'

# SSH Agent Autorun
# See: https://stackoverflow.com/questions/18880024/start-ssh-agent-on-login

SSH_ENV="$HOME/.ssh/environment"

function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' >|"${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    # shellcheck source=/dev/null
    . "${SSH_ENV}" >/dev/null
    /usr/bin/ssh-add
}

# Source SSH settings, if applicable

if [ -f "${SSH_ENV}" ]; then
    # shellcheck source=/dev/null
    . "${SSH_ENV}" >/dev/null
    # shellcheck disable=SC2009
    # ps ${SSH_AGENT_PID} doesn't work under cywgin
    ps -ef | grep "${SSH_AGENT_PID}" | grep ssh-agent$ >/dev/null || {
        start_agent
    }
else
    start_agent
fi

#-------------------------------------------------------------
# Local (non version controlled) settings and configuration
#-------------------------------------------------------------

if [ -f ~/.bashrc_site ]; then
    # shellcheck source=/dev/null
    . ~/.bashrc_site
fi

# Set up cargo environment (configured by rustup)

if [ -f "$HOME/.cargo/env" ]; then
    # shellcheck source=/dev/null
    . "$HOME/.cargo/env"
fi
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/shims:$PATH"

if which pyenv >/dev/null; then eval "$(pyenv init -)"; fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

export PATH=$PATH:$HOME/bin/opensearch-2.9.0/bin

# add Pulumi to the PATH
export PATH=$PATH:$HOME/.pulumi/bin

export PATH="$HOME/.luarocks/bin:$PATH"
export PATH="$PATH:/opt/nvim-linux-x86_64/bin"

function source_if_present() {
    if [ -f "$1" ]; then
        # shellcheck source=/dev/null
        . "$1"
    fi
}

source_if_present "$HOME/.asdf/asdf.sh"
source_if_present "$HOME/.asdf/completions/asdf.bash"
